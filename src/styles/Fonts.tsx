import { Global } from '@emotion/react'

export const Fonts = () => (
  <Global
    styles={[
      {
        '@font-face': {
          fontFamily: 'Kharkiv Tone',
          src: 'url("/assets/fonts/KharkivTone.ttf") format("truetype")',
        },
      },
    ]}
  />
)
